

module	VGA_Controller(	//	Host Side

						FPGA_RESET_n,
						MTL2_BL_ON_n,
//						iRed,
//						iGreen,
//						iBlue,
						oRequest,
						//	VGA Side
						oVGA_R,
						oVGA_G,
						oVGA_B,
						oVGA_H_SYNC,
						oVGA_V_SYNC,
						oVGA_SYNC,
						oVGA_BLANK,
						oVGA_CLOCK,
						//	Control Signal
						iCLK_30Mhz,
						iCLK_50Mhz,
//						iRST_N	
						
//						H_Cont,
//						V_Cont						
						
						);

`include "VGA_Param.h"

//	Host Side
//output 		[11:0]		H_Cont;
//output		[11:0]		V_Cont;
//
//input		[9:0]	iRed;
//input		[9:0]	iGreen;
//input		[9:0]	iBlue;
output	reg			oRequest;
//	VGA Side
output	reg[7:0]	oVGA_R;
output	reg[7:0]	oVGA_G;
output	reg[7:0]	oVGA_B;
output	reg			oVGA_H_SYNC;
output	reg			oVGA_V_SYNC;
output				oVGA_SYNC;
output				oVGA_BLANK;
output				oVGA_CLOCK;
//	Control Signal
input				iCLK_30Mhz;
input				iCLK_50Mhz;
//input				RESET;

input 		   FPGA_RESET_n;
output		   MTL2_BL_ON_n;


///////////////////////////////////////////////////////////////

//--RESET DELAY ---

reg   [31:0]  DELAY_CNT; 
reg           RESET ; 


always @(negedge FPGA_RESET_n or posedge iCLK_50Mhz ) begin 
if (!FPGA_RESET_n )  begin 
     RESET       <=0;
     DELAY_CNT   <=0;
end 
else  begin 
  if ( DELAY_CNT < 32'hfffff  )  DELAY_CNT<=DELAY_CNT+1; 
  else RESET<=1;
 end
end



assign  MTL2_BL_ON_n = ~RESET  ; 






//////////////////////////////////////////////////////////////

//	Internal Registers and Wires
reg		[11:0]		H_Cont;
reg		[11:0]		V_Cont;

assign	oVGA_BLANK	=	oVGA_H_SYNC & oVGA_V_SYNC;
assign	oVGA_SYNC	=	1'b0;
assign	oVGA_CLOCK	=	iCLK_30Mhz;

//assign	oVGA_R	=	(	H_Cont>=X_START 	&& H_Cont<X_START+H_SYNC_ACT &&
//						V_Cont>=Y_START 	&& V_Cont<Y_START+V_SYNC_ACT )square
//						?	8'b11110_1111	:	0;begin
//assign	oVGA_G	=	(	H_Cont>=X_START 	&& H_Cont<X_START+H_SYNC_ACT &&
//						V_Cont>=Y_START 	&& V_Cont<Y_START+V_SYNC_ACT )
//						?	8'b00000_00011	:	0;
//assign	oVGA_B	=	(	H_Cont>=X_START 	&& H_Cont<X_START+H_SYNC_ACT &&
//						V_Cont>=Y_START 	&& V_Cont<Y_START+V_SYNC_ACT )
//						?	8'b1100_0011	:	0;


wire [11:0] V_Cont_tmp;
wire [11:0] H_Cont_tmp;

assign V_Cont_tmp = (H_Cont>=X_START 	&& H_Cont<X_START+H_SYNC_ACT &&
					V_Cont>=Y_START 	&& V_Cont<Y_START+V_SYNC_ACT ) ? V_Cont - Y_START : {8{1'b0}};   //* compensate blank region to start calculate squares from zerro unlike calculating from Y_START -> ledds to wrong squares 

assign H_Cont_tmp = (H_Cont>=X_START 	&& H_Cont<X_START+H_SYNC_ACT &&
					V_Cont>=Y_START 	&& V_Cont<Y_START+V_SYNC_ACT ) ? H_Cont - X_START : {8{1'b0}};   //* compensate blank region to start calculate squares from zerro unlike calculating from Y_START -> ledds to wrong squares 

///////////////////////////////////////////////////////////
    always @(*) begin
        
        oVGA_R <= 8'b0000_0000;
        oVGA_G <= 8'b0000_0000;
        oVGA_B <= 8'b0000_0000;
		

        if(H_Cont>=X_START 	&& H_Cont<X_START+H_SYNC_ACT &&
					V_Cont>=Y_START 	&& V_Cont<Y_START+V_SYNC_ACT ) begin
			if (V_Cont_tmp[5] == 1) begin
				oVGA_R <= {{2{1'b0}},{6{H_Cont_tmp[5]}}};
				oVGA_B <= {8{~H_Cont_tmp[5]}};
			end else begin
				oVGA_R <= {8{~H_Cont_tmp[5]}};
				oVGA_B <= {{2{1'b0}},{6{H_Cont_tmp[5]}}};
			end	
        end
	end
///////////////////////////////////////////////////////////

//	Pixel LUT Address Generator
always@(posedge iCLK_30Mhz or negedge RESET)
begin
	if(!RESET)
	oRequest	<=	0;
	else
	begin
		if(	H_Cont>=X_START-2 && H_Cont<X_START+H_SYNC_ACT-2 &&
			V_Cont>=Y_START && V_Cont<Y_START+V_SYNC_ACT )
		oRequest	<=	1;
		else
		oRequest	<=	0;
	end
end

//	H_Sync Generator, Ref. 25.175 MHz Clock
always@(posedge iCLK_30Mhz or negedge RESET)
begin
	if(!RESET)
	begin
		H_Cont		<=	0;
		oVGA_H_SYNC	<=	0;
	end
	else
	begin
		//	H_Sync Counter
		if( H_Cont < H_SYNC_TOTAL )
		H_Cont	<=	H_Cont+1;
		else
		H_Cont	<=	0;
		//	H_Sync Generator
		if( H_Cont < H_SYNC_CYC )
		oVGA_H_SYNC	<=	0;
		else
		oVGA_H_SYNC	<=	1;
	end
end

//	V_Sync Generator, Ref. H_Sync
always@(posedge iCLK_30Mhz or negedge RESET)
begin
	if(!RESET)
	begin
		V_Cont		<=	0;
		oVGA_V_SYNC	<=	0;
	end
	else
	begin
		//	When H_Sync Re-start
		if(H_Cont==0)
		begin
			//	V_Sync Counter
			if( V_Cont < V_SYNC_TOTAL )
			V_Cont	<=	V_Cont+1;
			else
			V_Cont	<=	0;
			//	V_Sync Generator
			if(	V_Cont < V_SYNC_CYC )
			oVGA_V_SYNC	<=	0;
			else
			oVGA_V_SYNC	<=	1;
		end
	end
end

endmodule